package com.mvc.service.product;

import com.mvc.model.entity.ProductEntity;

/**
 * @author JayEinstein
 */
public interface ProductService {

    /**
     * 根据ID获得产品
     * @param productId
     * @return
     */
    ProductEntity getProductById(Long productId);

    /**
     * 扣减库存
     * @param productId
     * @param num
     * @return
     */
    boolean deductionStock(Long productId, Integer num);

    /**
     * 增加库存
     * @param productId
     * @param num
     * @return
     */
    boolean increaseStock(Long productId, Integer num);

}
