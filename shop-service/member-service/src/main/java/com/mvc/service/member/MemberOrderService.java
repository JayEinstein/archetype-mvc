package com.mvc.service.member;

import com.mvc.model.entity.OrderEntity;

import java.util.List;

/**
 * @author JayEinstein
 */
public interface MemberOrderService {

    /**
     * 获得用户的订单列表
     * @param memberId
     * @return
     */
    List<OrderEntity> listOrderByMemberId(Long memberId);

}
