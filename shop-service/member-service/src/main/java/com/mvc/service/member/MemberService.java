package com.mvc.service.member;

import com.mvc.model.entity.MemberEntity;

/**
 * @author JayEinstein
 */
public interface MemberService {

    /**
     * 根据ID获得会员
     * @param memberId
     * @return
     */
    MemberEntity getMemberById(Long memberId);

}
