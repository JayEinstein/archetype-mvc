package com.mvc.service.order;

/**
 * 订单连接测试
 * @author JayEinstein
 */
public interface OrderConnectTestService {

    void doMain();

    /**
     * 无返回
     */
    void notReturn();

    /**
     * 空字符串
     * @return
     */
    String strBlank();

    /**
     * 空格
     * @return
     */
    String strSpace();

    /**
     * 返回null
     * @return
     */
    Object objNull();

    /**
     * 除0异常
     * @return
     */
    int errorByZero();

}
