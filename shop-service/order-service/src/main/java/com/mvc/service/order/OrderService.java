package com.mvc.service.order;

import com.mvc.model.dto.OrderCreateDto;
import com.mvc.model.entity.OrderEntity;

import java.util.List;

/**
 * @author JayEinstein
 */
public interface OrderService {

    /**
     * 创建订单
     * @param createDto
     * @return
     */
    OrderEntity createOrder(OrderCreateDto createDto);

    /**
     * 根据ID获得订单
     * @param orderId
     * @return
     */
    OrderEntity getOrderById(Long orderId);

    /**
     * 更新订单状态为支付成功
     * @param orderId
     * @return
     */
    boolean updateOrderToPaid(Long orderId);

    /**
     * 更新订单状态为取消
     * @param orderId
     * @return
     */
    boolean updateOrderToCancel(Long orderId);

    /**
     * 更新订单状态为退款
     * @param orderId
     * @return
     */
    boolean updateOrderToRefund(Long orderId);

    /**
     * 获得用户订单
     * @param memberId
     * @return
     */
    List<OrderEntity> listOrderByMemberId(Long memberId);

}
