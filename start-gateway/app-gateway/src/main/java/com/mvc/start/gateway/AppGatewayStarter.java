package com.mvc.start.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 单体启动
 * @author JayEinstein
 */
@SpringBootApplication
public class AppGatewayStarter {

    public static void main(String[] args) {
        SpringApplication.run(AppGatewayStarter.class, args);
    }

}
