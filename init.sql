
CREATE TABLE IF NOT EXISTS `shop_member`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20)  NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE IF NOT EXISTS `shop_order`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `order_code` varchar(50) NULL DEFAULT NULL,
  `product_id` int(0) NULL DEFAULT NULL,
  `member_id` int(0) NULL DEFAULT NULL,
  `code` varchar(50) NULL DEFAULT NULL,
  `name` varchar(50) NULL DEFAULT NULL,
  `price` decimal(11, 0) NULL DEFAULT NULL,
  `status` varchar(20) NULL DEFAULT NULL,
  `pay_amount` decimal(11, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE IF NOT EXISTS `shop_product`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NULL DEFAULT NULL,
  `code` varchar(50) NULL DEFAULT NULL,
  `price` decimal(11, 0) NULL DEFAULT NULL,
  `stock` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `shop_member` VALUES (1, '小明');
INSERT INTO `shop_product` VALUES (1, '苹果', 'A0001', 88, 10);