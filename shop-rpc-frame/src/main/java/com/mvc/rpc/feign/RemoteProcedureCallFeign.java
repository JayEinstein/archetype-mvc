package com.mvc.rpc.feign;

import com.mvc.common.Res;
import com.mvc.rpc.CallMethod;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author JayEinstein
 */
@FeignClient(name = "meta-feign", fallbackFactory = RemoteProcedureCallFallback.class)
public interface RemoteProcedureCallFeign {

    /**
     * 远程服务方法
     * @param callMethod
     * @return
     */
    @PostMapping(CallMethod.CALL_URL)
    Res<String> call(@RequestBody CallMethod callMethod);

}
