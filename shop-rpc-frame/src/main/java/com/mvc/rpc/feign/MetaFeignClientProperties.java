package com.mvc.rpc.feign;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author JayEinstein
 */
@Component
@ConfigurationProperties(prefix = "meta.feign.client")
public class MetaFeignClientProperties {

    public static String PREFIX = "meta.feign.client";

    /**
     * 开启动态地址
     */
    private Boolean enableDynamic = true;

    private Map<String, FeignItem> mapper = new HashMap<>();

    /**
     * 子集合
     */
    private List<FeignItem> items = new ArrayList<>();

    public Boolean getEnableDynamic() {
        return enableDynamic;
    }

    public void setEnableDynamic(Boolean enableDynamic) {
        this.enableDynamic = enableDynamic;
    }

    public List<FeignItem> getItems() {
        return items;
    }

    public void setItems(List<FeignItem> items) {
        this.items = items;
    }

    public Map<String, FeignItem> getMapper() {
        return mapper;
    }

    public void setMapper(Map<String, FeignItem> mapper) {
        this.mapper = mapper;
    }
}
