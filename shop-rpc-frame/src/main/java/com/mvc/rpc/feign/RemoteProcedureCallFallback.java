package com.mvc.rpc.feign;

import com.mvc.common.Res;
import com.mvc.rpc.CallMethod;
import com.netflix.client.ClientException;
import com.netflix.hystrix.exception.HystrixTimeoutException;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * @author JayEinstein
 */
@Slf4j
public class RemoteProcedureCallFallback implements FallbackFactory<RemoteProcedureCallFeign> {

    @Override
    public RemoteProcedureCallFeign create(Throwable cause) {

        if (cause instanceof HystrixTimeoutException) {
            log.info("请求响应超时，{}");
        }

        Throwable cs = cause.getCause();
        if (cs instanceof ClientException) {
            log.info("负载均衡异常，{}", cs.getMessage());
        }


        log.error("feign fallback", cause);

        return new RemoteProcedureCallFeign() {

            @Override
            public Res<String> call(CallMethod callMethod) {
                return Res.timeout();
            }

        };
    }
}
