package com.mvc.rpc.feign;

import lombok.Data;

/**
 * @author JayEinstein
 */
@Data
public class FeignItem {

    /**
     * 开启代理
     */
    private Boolean enable = true;

    /**
     * 开启动态地址
     */
    private Boolean enableDynamic = true;

    /**
     * 静态地址
     */
    private String url;

    /**
     * 服务名称
     */
    private String serviceName;

    /**
     * 代理路径
     */
    private String proxyPackages;

}
