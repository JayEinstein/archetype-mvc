package com.mvc.rpc;

import com.alibaba.fastjson2.JSON;
import lombok.Data;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author JayEinstein
 */
@Data
public class CallMethod {

    /**
     * 远程调用路径
     */
    public static final String CALL_URL = "/remote/procedure/call";

    /**
     * 请求类
     */
    private String beanClass;

    /**
     * 请求方法
     */
    private String methodName;

    /**
     * 请求参数
     */
    private List<ArgHolder> argHolders;

    public Object[] parseArgValue(Type[] parameterTypes) {
        Object[] vals = new Object[parameterTypes.length];

        if (argHolders == null) {
            return vals;
        }

        for (int ndx = 0; ndx < parameterTypes.length; ndx++) {
            Type type = parameterTypes[ndx];
            ArgHolder argHolder = argHolders.get(ndx);
            String valStr = argHolder.getValStr();
            if (valStr != null) {
                vals[ndx] = JSON.parseObject(valStr, type);
            }
        }

        return vals;
    }

    /**
     * 是否有参数
     * @return
     */
    public boolean hadArg() {
        return argHolders != null && !argHolders.isEmpty();
    }

    /**
     * 获得参数类型
     * @return
     */
    public List<String> parseArgClassPath() {
        if (argHolders == null || argHolders.isEmpty()) {
            return null;
        }

        List<String> classNames = new ArrayList<>(argHolders.size());
        for (ArgHolder argHolder : argHolders) {
            String argClass = argHolder.getArgClass();
            classNames.add(argClass);
        }

        return classNames;
    }

}
