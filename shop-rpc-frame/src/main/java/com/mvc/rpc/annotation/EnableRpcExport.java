package com.mvc.rpc.annotation;

import com.mvc.rpc.RemoteProcedureCallExport;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启rpc代理方法出口
 * @author JayEinstein
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(RemoteProcedureCallExport.class)
public @interface EnableRpcExport {
}
