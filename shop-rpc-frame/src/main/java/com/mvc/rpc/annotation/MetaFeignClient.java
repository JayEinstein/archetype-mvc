package com.mvc.rpc.annotation;

import java.lang.annotation.*;

/**
 * 元Feign配置
 * @author JayEinstein
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface MetaFeignClient {

    /**
     * 开启代理
     * @return
     */
    boolean enable() default true;

    /**
     * 是否使用动态地址
     * @return
     */
    boolean dynamicAddr() default true;

    /**
     * 覆盖代理子类。开启时，所有代理接口的其他实现类都会停止注册。
     * @return
     */
    boolean coverProxy() default true;

}
