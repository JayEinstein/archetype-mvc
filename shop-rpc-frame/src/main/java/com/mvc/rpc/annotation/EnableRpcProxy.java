package com.mvc.rpc.annotation;

import com.mvc.rpc.RpcProxyImportSelector;
import com.mvc.rpc.feign.RemoteProcedureCallFeign;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 启用RPC代理
 * @author JayEinstein
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Repeatable(EnableRpcProxies.class)
@Import(RpcProxyImportSelector.class)
@EnableFeignClients(clients = RemoteProcedureCallFeign.class)
public @interface EnableRpcProxy {

    /**
     * host，直连地址
     * @return
     */
    @AliasFor("host")
    String value() default "";

    /**
     * host，直连地址
     * @return
     */
    @AliasFor("value")
    String host() default "";

    /**
     * 代理开闭
     * @return
     */
    boolean enable() default true;

    /**
     * 动态地址
     * @return
     */
    boolean dynamicAddr() default true;

    /**
     * 服务名称
     * @return
     */
    String serviceName();

    /**
     * 代理包路径
     * @return
     */
    String[] proxyPackages();

}
