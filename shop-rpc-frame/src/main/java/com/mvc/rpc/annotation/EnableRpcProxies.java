package com.mvc.rpc.annotation;

import com.mvc.rpc.RpcProxyImportSelector;
import com.mvc.rpc.feign.RemoteProcedureCallFeign;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * RPC代理集合
 * @author JayEinstein
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(RpcProxyImportSelector.class)
@EnableFeignClients(clients = RemoteProcedureCallFeign.class)
public @interface EnableRpcProxies {

    EnableRpcProxy[] value();

}
