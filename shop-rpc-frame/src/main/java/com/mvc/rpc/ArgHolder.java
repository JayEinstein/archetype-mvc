package com.mvc.rpc;

import com.alibaba.fastjson2.JSON;
import lombok.Data;

/**
 * 参数持有
 *
 * @author JayEinstein
 */
@Data
public class ArgHolder {

    private String argClass;

    private String valStr;

    public ArgHolder() {}

    public ArgHolder(Class<?> clz, Object val) {
        this.argClass = clz.getName();
        if (val != null) {
            this.valStr = JSON.toJSONString(val);
        }
    }

}