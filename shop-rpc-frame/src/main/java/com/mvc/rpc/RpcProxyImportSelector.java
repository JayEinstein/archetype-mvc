package com.mvc.rpc;

import com.mvc.rpc.annotation.MetaFeignClient;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;

/**
 * @author JayEinstein
 */
public class RpcProxyImportSelector implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {

        Map<String, Object> attributes = importingClassMetadata.getAnnotationAttributes(MetaFeignClient.class.getName());
        if (attributes == null) {
            return getImport();
        }
        AnnotationAttributes fromMap = AnnotationAttributes.fromMap(attributes);

        boolean enable = fromMap.getBoolean("enable");
        // enable = false 不开启注册
        if (!enable) {
            return new String[0];
        }

        return getImport();
    }

    private String[] getImport() {
        return new String[] { RpcProxyScannerRegister.class.getName() };
    }

}
