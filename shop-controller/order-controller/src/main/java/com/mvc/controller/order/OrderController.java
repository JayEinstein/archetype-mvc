package com.mvc.controller.order;

import com.mvc.common.Res;
import com.mvc.model.dto.OrderCreateDto;
import com.mvc.model.entity.OrderEntity;
import com.mvc.service.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author JayEinstein
 */
@RestController
@RequestMapping("/shop/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 创建订单
     * @param createDto
     * @return
     */
    @PostMapping("/create")
    public Res<OrderEntity> create(@RequestBody OrderCreateDto createDto) {
        /* 参数合法性校验 */
        Long memId = createDto.getMemId();
        if (memId == null) {
            throw new RuntimeException("会员Id不能为空");
        }
        Long productId = createDto.getProductId();
        if (productId == null) {
            throw new RuntimeException("购买的产品Id不能为空");
        }
        Integer buyNum = createDto.getBuyNum();
        if (buyNum == null) {
            throw new RuntimeException("购买的数量不能为空");
        }

        OrderEntity order = orderService.createOrder(createDto);

        return Res.ok(order);
    }

    /**
     * 订单支付
     * @param orderId
     * @return
     */
    @GetMapping("/paid")
    public Res paidSuccess(Long orderId) {

        boolean res = orderService.updateOrderToPaid(orderId);

        return Res.success(res);
    }

    /**
     * 获得用户的订单
     * @param memberId
     * @return
     */
    @GetMapping("/list/by/memberId")
    public Res<List<OrderEntity>> listOrderByMemberId(Long memberId) {
        List<OrderEntity> ls = orderService.listOrderByMemberId(memberId);
        return Res.ok(ls);
    }

}
