package com.mvc.controller.order;

import com.mvc.service.order.OrderConnectTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author JayEinstein
 */
@RestController
@RequestMapping("/shop/order/connect")
public class OrderConnectTestController {

    @Autowired
    private OrderConnectTestService orderConnectTestService;

    @GetMapping("/test")
    public Object feignConnectTest() {

        orderConnectTestService.doMain();

        return "everything is ok";
    }

}
