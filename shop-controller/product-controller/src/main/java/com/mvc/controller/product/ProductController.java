package com.mvc.controller.product;

import com.mvc.common.Res;
import com.mvc.model.dto.StockDto;
import com.mvc.model.entity.ProductEntity;
import com.mvc.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author JayEinstein
 */
@RestController
@RequestMapping("/shop/product")
public class ProductController {

    @Autowired
    private ProductService productService;


    @GetMapping("/get")
    public Res<ProductEntity> getProductById(Long productId) {

        ProductEntity productEntity = productService.getProductById(productId);

        return Res.ok(productEntity);

    }

    @PostMapping("/deduction/stock")
    public Res<Boolean> deductionStock(@RequestBody StockDto stockDto) {

        boolean res = productService.deductionStock(stockDto.getProductId(), stockDto.getNum());

        return Res.success(res);
    }

    @PostMapping("/increase/stock")
    public Res<Boolean> increaseStock(@RequestBody StockDto stockDto) {

        boolean res = productService.increaseStock(stockDto.getProductId(), stockDto.getNum());

        return Res.success(res);
    }


}
