package com.mvc.controller.member;

import com.mvc.common.Res;
import com.mvc.model.entity.MemberEntity;
import com.mvc.model.entity.OrderEntity;
import com.mvc.service.member.MemberOrderService;
import com.mvc.service.member.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author JayEinstein
 */
@RestController
@RequestMapping("/shop/member")
public class MemberController {

    @Autowired
    private MemberService memberService;

    @Autowired
    private MemberOrderService memberOrderService;

    @GetMapping("/get")
    public Res<MemberEntity> getMemberById(Long memberId) {

        MemberEntity member = memberService.getMemberById(memberId);

        return Res.ok(member);
    }

    @GetMapping("/order/list")
    public Res<List<OrderEntity>> listOrderByMemberId(Long memberId) {

        List<OrderEntity> ls = memberOrderService.listOrderByMemberId(memberId);

        return Res.ok(ls);
    }

}
