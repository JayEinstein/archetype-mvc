package com.mvc.service.member.dao;

import com.mvc.dao.mapper.MemberEntityMapper;
import com.mvc.model.entity.MemberEntity;
import com.mvc.service.member.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author JayEinstein
 */
@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberEntityMapper memberEntityMapper;

    @Override
    public MemberEntity getMemberById(Long memberId) {
        return memberEntityMapper.selectById(memberId);
    }

}
