package com.mvc.service.member.dao;

import com.mvc.model.entity.OrderEntity;
import com.mvc.service.member.MemberOrderService;
import com.mvc.service.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author JayEinstein
 */
@Service
public class MemberOrderServiceImpl implements MemberOrderService {

    @Autowired
    private OrderService orderService;

    @Override
    public List<OrderEntity> listOrderByMemberId(Long memberId) {
        return orderService.listOrderByMemberId(memberId);
    }

}
