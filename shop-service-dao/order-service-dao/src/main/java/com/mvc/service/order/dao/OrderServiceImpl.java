package com.mvc.service.order.dao;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mvc.common.ResultCode;
import com.mvc.common.tools.AssertFuse;
import com.mvc.dao.mapper.OrderEntityMapper;
import com.mvc.model.dto.OrderCreateDto;
import com.mvc.model.entity.MemberEntity;
import com.mvc.model.entity.OrderEntity;
import com.mvc.model.entity.ProductEntity;
import com.mvc.model.enums.OrderStatusEnum;
import com.mvc.service.member.MemberService;
import com.mvc.service.order.OrderService;
import com.mvc.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author JayEinstein
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderEntityMapper orderEntityMapper;

    @Autowired
    private MemberService memberService;

    @Autowired
    private ProductService productService;

    @Override
    public OrderEntity createOrder(OrderCreateDto createDto) {
        Long memId = createDto.getMemId();
        Long productId = createDto.getProductId();
        Integer buyNum = createDto.getBuyNum();

        /* 参数有效性校验 */
        MemberEntity memberEntity = memberService.getMemberById(memId);
        if (memberEntity == null) {
            throw AssertFuse.of(ResultCode.ILLEGAL_PARAMETER, String.format("会员信息错误，%s", memId));
        }

        ProductEntity productEntity = productService.getProductById(productId);
        if (productEntity == null) {
            throw AssertFuse.of(ResultCode.ILLEGAL_PARAMETER, "购买的产品不存在");
        }

        /* 扣除库存 */
        boolean deductionRes = productService.deductionStock(productId, buyNum);
        if (!deductionRes) {
            throw AssertFuse.of(ResultCode.ILLEGAL_PARAMETER, String.format("%s库存不足", productEntity.getName()));
        }

        OrderEntity orderEntity = new OrderEntity();
        BeanUtil.copyProperties(productEntity, orderEntity);
        orderEntity.setId(null);
        orderEntity.setProductId(productEntity.getId());
        orderEntity.setMemberId(memId);
        orderEntity.setStatus(OrderStatusEnum.WAIT.name());
        orderEntity.setOrderCode(System.currentTimeMillis() + "");

        /* 计算订单价格 */
        BigDecimal payAmount = productEntity.getPrice().multiply(BigDecimal.valueOf(buyNum));
        orderEntity.setPayAmount(payAmount);

        /* 保存订单 */
        orderEntityMapper.insert(orderEntity);
        return orderEntity;
    }

    @Override
    public OrderEntity getOrderById(Long orderId) {
        return orderEntityMapper.selectById(orderId);
    }

    @Override
    public boolean updateOrderToPaid(Long orderId) {

        OrderEntity update = new OrderEntity();
        update.setId(orderId);
        update.setStatus(OrderStatusEnum.PAID.name());

        return orderEntityMapper.updateById(update) > 0;
    }

    @Override
    public boolean updateOrderToCancel(Long orderId) {
        OrderEntity update = new OrderEntity();
        update.setId(orderId);
        update.setStatus(OrderStatusEnum.CANCEL.name());

        return orderEntityMapper.updateById(update) > 0;
    }

    @Override
    public boolean updateOrderToRefund(Long orderId) {
        OrderEntity update = new OrderEntity();
        update.setId(orderId);
        update.setStatus(OrderStatusEnum.REFUND.name());

        return orderEntityMapper.updateById(update) > 0;
    }

    @Override
    public List<OrderEntity> listOrderByMemberId(Long memberId) {

        QueryWrapper<OrderEntity> query = new QueryWrapper<>();
        query.lambda().eq(OrderEntity::getMemberId, memberId);

        return orderEntityMapper.selectList(query);
    }

}
