package com.mvc.service.order.dao;

import com.alibaba.fastjson2.JSON;
import com.mvc.common.Res;
import com.mvc.common.ResultCode;
import com.mvc.common.excetion.FeignResponseException;
import com.mvc.common.excetion.ResThrow;
import com.mvc.common.tools.AssertFuse;
import com.mvc.service.order.OrderConnectTestService;
import com.mvc.service.product.ProductConnectTestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author JayEinstein
 */
@Service
@Slf4j
public class OrderConnectTestServiceImpl implements OrderConnectTestService {

    @Autowired
    private ProductConnectTestService productConnectTestService;

    @Override
    public void doMain() {

        try {

            Method[] declaredMethods = ReflectionUtils.getDeclaredMethods(OrderConnectTestService.class);
            for (Method declaredMethod : declaredMethods) {

                if ("doMain".equals(declaredMethod.getName())) {
                    continue;
                }

                declaredMethod.invoke(this);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void notReturn() {
        productConnectTestService.notReturn();
        log.info("notReturn is ok");
    }

    @Override
    public String strBlank() {
        String strBlank = productConnectTestService.strBlank();

        if (!"".equals(strBlank)) {
            log.error("strBlank test warn，result: {}", strBlank);
        } else {
            log.info("strBlank is ok");
        }

        return strBlank;
    }

    @Override
    public String strSpace() {
        String strSpace = productConnectTestService.strSpace();
        if (!" ".equals(strSpace)) {
            log.error("strSpace test warn，result: {}", strSpace);
        } else {
            log.info("strSpace is ok");
        }
        return strSpace;
    }

    @Override
    public Object objNull() {
        Object objNull = productConnectTestService.objNull();
        if (objNull != null) {
            log.error("objNull test warn，result: {}", objNull);
        } else {
            log.info("objNull is ok");
        }
        return objNull;
    }

    @Override
    public int errorByZero() {

        try {
            // 模拟系统异常情况
            return productConnectTestService.errorByZero();
        } catch (ResThrow e) {

            Res res = e.getRes();
            String json = JSON.toJSONString(res);

            if (ResultCode.ERROR.is(e.getCode())) {
                log.info("errorByZero is ok，{}", json);
            } else {
                log.error("errorByZero test warn，result: {}", json);
            }

            return 0;

        } catch (Exception e) {
            throw e;
        }

    }


}
