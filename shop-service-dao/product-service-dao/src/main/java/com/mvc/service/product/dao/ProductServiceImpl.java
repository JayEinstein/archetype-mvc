package com.mvc.service.product.dao;

import com.mvc.dao.mapper.ProductEntityMapper;
import com.mvc.model.entity.ProductEntity;
import com.mvc.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author JayEinstein
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductEntityMapper productEntityMapper;

    @Override
    public ProductEntity getProductById(Long productId) {
        return productEntityMapper.selectById(productId);
    }

    @Override
    public boolean deductionStock(Long productId, Integer num) {
        return productEntityMapper.deductionStock(productId, num);
    }

    @Override
    public boolean increaseStock(Long productId, Integer num) {
        return productEntityMapper.increaseStock(productId, num);
    }

}
