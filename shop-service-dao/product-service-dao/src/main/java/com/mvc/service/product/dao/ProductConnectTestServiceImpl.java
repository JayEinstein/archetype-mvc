package com.mvc.service.product.dao;

import com.mvc.service.product.ProductConnectTestService;
import org.springframework.stereotype.Service;

/**
 * @author JayEinstein
 */
@Service
public class ProductConnectTestServiceImpl implements ProductConnectTestService {

    @Override
    public void notReturn() {
        System.out.println("notReturn...");
    }

    @Override
    public String strBlank() {
        System.out.println("strBlank...");
        return "";
    }

    @Override
    public String strSpace() {
        System.out.println("strSpace...");
        return " ";
    }

    @Override
    public Object objNull() {
        System.out.println("objNull...");
        return null;
    }

    @Override
    public int errorByZero() {
        System.out.println("1/0...");
        return 1/0;
    }

}
