package com.mvc.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

/**
 * mysql数据表实体类
 * @author JayEinstein
 */
@Data
@TableName("shop_product")
public class ProductEntity {

    /**
     * id
     */
    @TableId
    private Long id;

    /**
     * 产品编码
     */
    private String code;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 产品价格
     */
    private BigDecimal price;

    /**
     * 产品库存
     */
    private Integer stock;

}
