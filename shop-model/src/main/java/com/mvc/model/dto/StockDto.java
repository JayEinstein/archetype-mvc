package com.mvc.model.dto;

import lombok.Data;

/**
 * 库存操作
 * @author JayEinstein
 */
@Data
public class StockDto {

    private Long productId;

    private Integer num;

}
