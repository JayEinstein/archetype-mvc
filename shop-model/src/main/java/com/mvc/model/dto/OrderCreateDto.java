package com.mvc.model.dto;

import lombok.Data;

/**
 * @author JayEinstein
 */
@Data
public class OrderCreateDto {

    /**
     * 会员ID
     */
    private Long memId;

    /**
     * 构建的产品ID
     */
    private Long productId;

    /**
     * 购买数量
     */
    private Integer buyNum;

}
