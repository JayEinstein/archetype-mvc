package com.mvc.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvc.model.entity.OrderEntity;

/**
 * @author JayEinstein
 */
public interface OrderEntityMapper extends BaseMapper<OrderEntity> {
}
