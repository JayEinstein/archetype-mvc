package com.mvc.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvc.model.entity.MemberEntity;

/**
 * @author JayEinstein
 */
public interface MemberEntityMapper extends BaseMapper<MemberEntity> {
}
