package com.mvc.common;

import lombok.Getter;

/**
 * 服务通用状态码
 * @author JayEinstein
 */
@Getter
public enum ResultCode implements StatusCode {

    /**
     * 服务状态码，"0" 之外表示请求没有按预期的进行
     */
    BUSINESS_SUCCESS("0", "请求处理成功"),

    /**
     * 请求成功，但是业务处理失败。
     */
    BUSINESS_FAILURE("1", "请求处理失败"),




    /* A0001 用户方操作引起的失败 */

    NO_PERMISSION("A0001", "无访问权限"),

    ILLEGAL_PARAMETER("A0002", "参数错误"),




    /* B0001 系统内部服务异常 */
    ERROR("B0001", "系统异常"),
    BUSY("B0002", "系统繁忙"),
    WARN("B0003", "异常警告"),



    /* C0001 调用内部服务出错 */
    TIMEOUT("C0001", "请求超时"),
    BUSY_OF_INSIDE("C0002", "系统繁忙"),

    NOT_FOUND_FOR_INSIDE("C0400", "请求路径异常"),
    ERROR_OF_INSIDE("C0500", "系统异常"),

    /* D0001 调用第三方服务出错 */

    TIMEOUT_OF_OUTSIDE("D0001", "第三方请求超时"),
    BUSY_OF_OUTSIDE("D0002", "第三方系统繁忙"),

    NOT_FOUND_FOR_OUTSIDE("D0400", "第三方路径不存在"),
    ERROR_OF_OUTSIDE("D0500", "第三方系统异常"),


    ;

    private String code;

    private String msg;

    ResultCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 请求成功
     * @param code
     * @return
     */
    public static boolean isSuccess(String code) {
        return BUSINESS_SUCCESS.is(code) || BUSINESS_FAILURE.is(code);
    }

    /**
     * 是否是当前状态码
     * @param code 比较状态码
     * @return 是否是当前状态码
     */
    public boolean is(String code) {
        return this.code.equals(code);
    }

}
