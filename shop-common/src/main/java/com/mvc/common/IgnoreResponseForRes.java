package com.mvc.common;

import java.lang.annotation.*;

/**
 * 忽略返回结果封装
 * @author JayEinstein
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreResponseForRes {
}
