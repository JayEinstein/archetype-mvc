package com.mvc.common.tools;

import com.mvc.common.Res;
import com.mvc.common.StatusCode;
import com.mvc.common.excetion.AssertDataThrow;
import com.mvc.common.excetion.AssertThrow;
import com.mvc.common.excetion.ResThrow;
import com.mvc.common.validate.ParamChecker;

/**
 * 断点熔断器
 * @author JayEinstein
 */
public abstract class AssertFuse {

    /**
     * 抛出res
     * @param res Res
     */
    public static ResThrow of(Res res) {
        throw new ResThrow(res);
    }

    /**
     * 产生熔断
     * @param statusCode 熔断状态
     */
    public static AssertThrow of(StatusCode statusCode) {
        throw new AssertThrow(statusCode);
    }

    /**
     * 产生熔断
     * @param statusCode 熔断状态
     * @param msg 熔断信息
     */
    public static AssertThrow of(StatusCode statusCode, String msg) {
        throw new AssertThrow(statusCode, msg);
    }

    /**
     * 产生熔断
     * @param statusCode 熔断状态
     * @param data 熔断数据
     */
    public static AssertThrow of(StatusCode statusCode, Object data) {
        throw new AssertDataThrow(statusCode, data);
    }

    /**
     * 产生熔断
     * @param statusCode 熔断状态
     * @param data 熔断数据
     * @param msg 熔断信息
     */
    public static AssertDataThrow of(StatusCode statusCode, Object data, String msg) {
        throw new AssertDataThrow(statusCode, data, msg);
    }

    /**
     * 熔断校验（参数）
     */
    public static void validate() {
        // TODO
    }

    /**
     * 参数校验器
     */
    private static ParamChecker paramChecker = new ParamChecker();

    public static ParamChecker getChecker() {
        if (paramChecker != null) {
            return paramChecker;
        }
        paramChecker = new ParamChecker();
        return paramChecker;
    }

}
