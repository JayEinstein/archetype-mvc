package com.mvc.common.validate;

import cn.hutool.core.util.StrUtil;
import com.mvc.common.ResultCode;
import com.mvc.common.tools.AssertFuse;

/**
 * 参数校验器
 * @author JayEinstein
 */
public class ParamChecker {

    public ParamChecker notNull(Object value) {
        if (value == null) {
            throw AssertFuse.of(ResultCode.ILLEGAL_PARAMETER, "参数不能为空");
        }
        return this;
    }

    public ParamChecker notBlank(String value) {
        if (StrUtil.isBlank(value)) {
            throw AssertFuse.of(ResultCode.ILLEGAL_PARAMETER);
        }
        return this;
    }

}
