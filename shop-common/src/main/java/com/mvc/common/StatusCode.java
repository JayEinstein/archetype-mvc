package com.mvc.common;

/**
 * 状态码
 * @author JayEinstein
 */
public interface StatusCode {

    /**
     * 状态码
     * @return 状态码
     */
    String getCode();

    /**
     * 描述信息
     * @return 描述信息
     */
    String getMsg();

}
