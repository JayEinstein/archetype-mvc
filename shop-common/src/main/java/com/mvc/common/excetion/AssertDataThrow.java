package com.mvc.common.excetion;

import com.mvc.common.StatusCode;

/**
 * 带有数据的断点抛出
 * @author JayEinstein
 */
public class AssertDataThrow extends AssertThrow {

    /**
     * 断点数据
     */
    private Object data;

    public AssertDataThrow(StatusCode statusCode) {
        super(statusCode);
    }

    public AssertDataThrow(StatusCode statusCode, String msg) {
        super(statusCode, msg);
    }

    public AssertDataThrow(StatusCode statusCode, Object data) {
        super(statusCode);
        if (data == null) {
            return;
        }
        this.data = data;
    }

    public AssertDataThrow(StatusCode statusCode, Object data, String msg) {
        super(statusCode, msg);
        if (data == null) {
            return;
        }
        this.data = data;
    }

    /**
     * 数据类型
     * @return
     */
    public Class getDataClassType() {
        if (data == null) {
            return null;
        }
        return data.getClass();
    }

    /**
     * 数据信息
     * @param <T>
     * @return
     */
    public <T> T getData() {
        if (data == null) {
            return null;
        }
        return (T) data;
    }

}
