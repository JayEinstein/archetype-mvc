package com.mvc.common.excetion;

import com.mvc.common.Res;

/**
 * Res 异常抛出
 * @author JayEinstein
 */
public class ResThrow extends NotStackTraceException {

    private Res res;

    public ResThrow(Res res) {
        super(res.getMsg());
        this.res = res;
    }

    public Res getRes() {
        return res;
    }

    public String getCode() {
        return res.getCode();
    }

    public String getMsg() {
        return res.getMsg();
    }

    public Object getData() {
        return res.getData();
    }

}
