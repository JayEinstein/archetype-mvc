package com.mvc.common.excetion;

/**
 * feign 响应异常
 * @author JayEinstein
 */
public class FeignResponseException extends NotStackTraceException {

    /**
     * 响应码 {@link Code}
     */
    private int statusCode;

    /**
     * 响应体
     */
    private String responseBody;

    public FeignResponseException(int statusCode) {
        this.statusCode = statusCode;
    }

    public FeignResponseException(int statusCode, String responseBody) {
        super(responseBody);
        this.statusCode = statusCode;
        this.responseBody = responseBody;
    }

    public FeignResponseException(int statusCode, Throwable cause) {
        super(cause);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    /**
     * feign错误码集合
     */
    public interface Code {

        /**
         * 超时
         */
        int TIMEOUT = -1;

        /**
         * 负载均衡错误
         */
        int CLIENT_ERROR = -2;

        /**
         * 未知错误
         */
        int UNKNOWN = Integer.MIN_VALUE;
    }

}
