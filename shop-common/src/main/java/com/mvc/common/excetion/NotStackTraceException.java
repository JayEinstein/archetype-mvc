package com.mvc.common.excetion;

/**
 * 没有堆栈的信息的异常。
 * @author JayEinstein
 */
public class NotStackTraceException extends RuntimeException {

    public NotStackTraceException() {
    }

    public NotStackTraceException(String message) {
        super(message);
    }

    public NotStackTraceException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotStackTraceException(Throwable cause) {
        super(cause);
    }

    /**
     * 不进行堆栈填充，所以!!!!!!!!!!这是一个不会打印堆栈的异常!!!!!!!!!!
     * @return
     */
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

}
