package com.mvc.common.excetion;

import com.mvc.common.ResultCode;
import com.mvc.common.StatusCode;

/**
 * 抛出断点
 * @author JayEinstein
 */
public class AssertThrow extends NotStackTraceException {

    /**
     * 响应码
     */
    private StatusCode statusCode;

    /**
     * 断点描述
     */
    private String msg;

    public AssertThrow(StatusCode statusCode) {
        super(statusCode.getMsg());
        this.statusCode = statusCode;
        this.msg = statusCode.getMsg();
    }

    public AssertThrow(StatusCode statusCode, String msg) {
        super(msg);
        this.statusCode = statusCode;
        this.msg = msg;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public String getCode() {
        return statusCode.getCode();
    }

    public String getMsg() {
        return msg;
    }

}
