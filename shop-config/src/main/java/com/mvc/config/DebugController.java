package com.mvc.config;

import com.mvc.config.yml.CustomProperties;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author JayEinstein
 */
@RestController
@RequestMapping("/shop/config")
public class DebugController implements ApplicationContextAware {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired(required = false)
    private CustomProperties customProperties;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @GetMapping("/debug")
    public Object debug() {

        return "everything is ok";
    }

}
