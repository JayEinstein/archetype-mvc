package com.mvc.config.yml;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * 导入自定义yml
 * @author JayEinstein
 */
@Component
@PropertySource(value = "classpath:custom.yml", factory = YamlPropertySourceFactory.class)
@ConfigurationProperties(prefix = "custom")
@Data
@ConditionalOnResource(resources = "classpath:custom.yml")
public class CustomProperties implements EnvironmentAware {

    private String name;

    private Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
