package com.mvc.config.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author JayEinstein
 */
@Component
@Slf4j
public class BaseFeignInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {

        log.info("feign 拦截，{}", template.request().url());

        template.header("interceptTag", "true");

    }

}
