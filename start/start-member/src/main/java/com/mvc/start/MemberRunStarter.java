package com.mvc.start;

import com.mvc.rpc.annotation.EnableRpcExport;
import com.mvc.rpc.annotation.EnableRpcProxy;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 单体启动
 * @author JayEinstein
 */
@SpringBootApplication
@MapperScan("com.mvc.dao.mapper")
@ComponentScan(basePackages = {
        "com.mvc.controller",
        "com.mvc.service",
        "com.mvc.config"
})
@EnableRpcExport
@EnableRpcProxy(serviceName = "start-order", value = "http://localhost:9003", proxyPackages = "com.mvc.service.order", enable = true)
public class MemberRunStarter {

    public static void main(String[] args) {
        SpringApplication.run(MemberRunStarter.class, args);
    }

}