package com.mvc.start;

import com.mvc.rpc.annotation.EnableRpcExport;
import com.mvc.rpc.annotation.EnableRpcProxy;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 单体启动
 * @author JayEinstein
 */
@SpringBootApplication
@MapperScan("com.mvc.dao.mapper")
@ComponentScan(basePackages = {
        "com.mvc.controller",
        "com.mvc.service",
        "com.mvc.config"
})
@EnableRpcExport
@EnableRpcProxy(serviceName = "start-product", value = "http://localhost:9001", proxyPackages = "com.mvc.service.product")
@EnableRpcProxy(serviceName = "start-member", value = "http://localhost:9002", proxyPackages = "com.mvc.service.member")
public class OrderRunStarter {

    public static void main(String[] args) {
        SpringApplication.run(OrderRunStarter.class, args);
    }

}