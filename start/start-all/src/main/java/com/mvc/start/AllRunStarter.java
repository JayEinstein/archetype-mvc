package com.mvc.start;

import com.mvc.rpc.annotation.EnableRpcProxy;
import com.mvc.rpc.annotation.MetaFeignClient;
import com.mvc.rpc.feign.MetaFeignAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * 单体启动
 * @author JayEinstein
 */
@SpringBootApplication
@MapperScan("com.mvc.dao.mapper")
@ComponentScan(basePackages = {
        "com.mvc.controller",
        "com.mvc.service",
        "com.mvc.config"
})
@MetaFeignClient
@EnableRpcProxy(serviceName = "start-product", value = "http://localhost:9001", proxyPackages = "com.mvc.service.product")
@EnableRpcProxy(serviceName = "start-member", value = "http://localhost:9002", proxyPackages = "com.mvc.service.member", enable = false)
@EnableRpcProxy(serviceName = "start-order", value = "http://localhost:9003", proxyPackages = "com.mvc.service.order", enable = false)
public class AllRunStarter {

    public static void main(String[] args) {
        SpringApplication.run(AllRunStarter.class, args);
    }

}