package com.mvc.start;

import com.mvc.rpc.annotation.EnableRpcExport;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 单体启动
 * @author JayEinstein
 */
@SpringBootApplication
@MapperScan("com.mvc.dao.mapper")
@ComponentScan(basePackages = {
        "com.mvc.controller",
        "com.mvc.service",
        "com.mvc.config"
})
@EnableRpcExport
public class ProductRunStarter {

    public static void main(String[] args) {
        SpringApplication.run(ProductRunStarter.class, args);
    }

}